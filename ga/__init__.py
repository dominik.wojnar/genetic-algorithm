from math import floor
from operator import attrgetter
import random
from random import shuffle

NULL = 'NOT_CALCULATED'


class GenomeInstance:

    def __init__(self, genomVal, fitness):
        self.genomVal = genomVal
        self.fitness = fitness

    def __str__(self):
        return "" + str(self.genomVal) + " " + str(self.fitness)

    def __add__(self, other):
        if isinstance(other, GenomeInstance):
            belongArr = []
            newGenome = []
            size = len(self.genomVal)
            for i in range(size):
                if i < (size / 2):
                    belongArr.append(self)
                else:
                    belongArr.append(other)
            shuffle(belongArr)
            for i in range(size):
                newGenome.append(belongArr[i].genomVal[i])
            return GenomeInstance(newGenome, NULL)
        else:
            raise ValueError("GenomeInstance is required")

    def randomize(self, mutationRate, step):
        size = len(self.genomVal)
        for i in range(size):
            mutation = mutationRate *  self.genomVal[i] * random.random() / step
            mutation -= floor (mutation / 2)
            self.genomVal[i] += (mutation*step)


class GenomeGenerator:
    def __init__(self, genomLen, minVal, maxVal, step, poolSize, mutationRate, numOfLeaders,
                 fitnessFunction):
        self.genomLen = genomLen
        self.minVal = minVal
        self.maxVal = maxVal
        self.step = step
        self.poolSize = poolSize
        self.mutationRate = mutationRate
        self.numOfLeaders = numOfLeaders
        self.pool = []
        self.fitnessFunction = fitnessFunction
        for i in range(poolSize):
            self.pool.append(self.nextGenom())
        self.calcFitness()


    def calcFitness(self):
        for i in range(self.poolSize):
            tmp = self.fitnessFunction(self.pool[i])
            self.pool[i].fitness = tmp
        self.pool = sorted(self.pool, key=attrgetter('fitness'))


    def log(self):
        for i in range(self.poolSize):
            print(self.pool[i])


    def nextGeneration(self):
        leaders = []
        for i in range(self.numOfLeaders):
            leaders.append(self.pool[i])
        self.pool.clear()
        for i in range(self.poolSize):
            self.pool.append(leaders[i % self.numOfLeaders])
            if self.numOfLeaders < i:
                self.pool[i] = self.pool[i] + self.pool[int(i / self.numOfLeaders)]
                self.pool[i].randomize(self.mutationRate, self.step)
        self.calcFitness()
        self.pool = sorted(self.pool, key=attrgetter('fitness'))


    def nextGenom(self):
        genomVal = []
        for i in range(self.genomLen):
            genomVal.append(random.uniform(self.minVal, self.maxVal)) # self.step
        return GenomeInstance(genomVal, NULL)

class GeneticAlgorithm:
    def __init__(self, genomeGenerator):
        self.genomeGenerator = genomeGenerator
