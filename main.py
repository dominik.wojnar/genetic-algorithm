import ga


def fitness(genome):
    return abs(-3* genome.genomVal[0] ** 6 + 7* genome.genomVal[1] ** 5 - 0.33*genome.genomVal[3] ** 4 +genome.genomVal[4] ** 3 + genome.genomVal[5] ** 2 + 3 * genome.genomVal[7] - 35)


gg = ga.GenomeGenerator(12, -100, 100, 0.1, 20, 0.01, 2, fitness)
print(gg.pool[0].fitness)
for i in range(10000):
    gg.nextGeneration()
print(gg.pool[0].fitness)
